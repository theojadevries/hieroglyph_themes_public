"""HTML5 slides theme for ReStructured Text documents, served from a (configurable) webserver
"""
from os import path

__version__ = '0.1'
__version_full__ = __version__
name = "hieroglyph_fromserver_theme"


def get_html_theme_path():
    """Return list of HTML theme paths."""
    cur_dir = path.abspath(path.dirname(path.dirname(__file__)))
    return cur_dir


def setup(app):
    app.add_html_theme('hieroglyph_fromserver_theme', path.abspath(path.dirname(__file__)))