# -*- coding: utf-8 -*-
"""`hieroglyph_fromserver_theme` lives on `Github`_.

.. _github: https://github.com/
"""
from io import open
from setuptools import setup
# from hieroglyph_fromserver_theme import __version__

with open('README.rst', 'r') as f:
        long_description = f.read()

setup(
    name='hieroglyph_fromserver_theme',
    version='0.1',
#    version=__version__,
    zip_safe=False,
    install_requires=[
            'setuptools',
            'hieroglyph>=0.6',
            ],
    license='MIT',
    author='Theo J.A. de Vries',
    author_email='t.j.a.devries@gmail.com',
    description='HTML5 slides theme for ReStructured Text documents, served from a (configurable) webserver',
    long_description=long_description,
    url = 'https://bitbucket.org/theojadevries/hieroglyph_themes_public'
    packages=['hieroglyph_fromserver_theme'],
    package_data={'hieroglyph_fromserver_theme': [
        'theme.conf',
        '*.html',
        'static/*.js_t',
    ]},
    include_package_data=True,
    # See http://www.sphinx-doc.org/en/stable/theming.html#distribute-your-theme-as-a-python-package
    entry_points = {
        'hieroglyph.html_themes': [
            'hieroglyph_fromserver_theme = hieroglyph_fromserver_theme',
        ]
    },
    classifiers=[
        'Framework :: Sphinx',
        'Framework :: Sphinx :: Extension',
        'Framework :: Sphinx :: Theme',
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: MIT License',
        'Environment :: Console',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'Intended Audience :: End Users/Desktop',
        'Programming Language :: Python',
        'Operating System :: OS Independent',
        'Topic :: Documentation :: Sphinx',
        'Topic :: Text Processing :: Markup',
    ],
)
